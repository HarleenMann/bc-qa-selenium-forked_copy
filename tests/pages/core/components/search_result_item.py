from selenium.webdriver.common.by import By
from pypom import Region

class SearchResultItem(Region):

    _bib_title_locator = (By.CSS_SELECTOR, "[data-key='bib-title']")
    _add_to_shelf_combo_button_locator = (By.CLASS_NAME, "cp-add-to-shelf-combo-button")
    _add_to_shelf_success_message_locator = (By.CLASS_NAME, "cp-alert-success")
    _on_shelf_button_locator = (By.CLASS_NAME, "cp-on-shelf-button")
    # _on_shelf_dropdown_button_locator = (By.CLASS_NAME, "cp-on-shelf-dropdown")
    _on_shelf_dropdown_button_locator = (By.CLASS_NAME, "cp-dropdown-trigger")
    _remove_from_shelves_locator = (By.CLASS_NAME, "shelf-delete")

    @property
    def bib_title(self):
        return self.find_element(*self._bib_title_locator)

    @property
    def add_to_shelf_combo_button(self):
        return self.find_element(*self._add_to_shelf_combo_button_locator)

    @property
    def add_to_shelf_success_message(self):
        return self.find_element(*self._add_to_shelf_success_message_locator)

    @property
    def is_add_to_shelf_success_message_displayed(self):
        return self.find_element(*self._add_to_shelf_success_message_locator).is_displayed()

    @property
    def on_shelf_button(self):
        return self.find_element(*self._on_shelf_button_locator)

    @property
    def is_on_shelf_button_displayed(self):
        return self.find_element(*self._on_shelf_button_locator).is_displayed()

    @property
    def on_shelf_dropdown_button(self):
        return self.find_element(*self._on_shelf_dropdown_button_locator)

    @property
    def remove_from_shelves(self):
        return self.find_element(*self._remove_from_shelves_locator)
