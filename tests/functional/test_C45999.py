from selenium import webdriver
import configuration.system
import configuration.chrome
import configuration.user
from mamba import description, it
import sure
from pages.core.home import HomePage
from pages.core.v2.search_results import SearchResultsPage
import re

with description("Core Automation Candidates:") as self:
    with before.all:
        self.driver = webdriver.Chrome(executable_path = configuration.chrome.driver_path, chrome_options = configuration.chrome.options)
        self.driver.switch_to_window(self.driver.current_window_handle)

    with after.all:
        self.driver.quit

    with _it("C45999: Shelves Page Actions"):
        home_page = HomePage(self.driver, configuration.system.base_url).open()
